package com.vividmindsoft.mvvm_rx;

import android.text.TextUtils;
import android.util.Patterns;

import com.vividmindsoft.mvvm_rx.enums.resultCodes.LoginOutResultCodes;
import com.vividmindsoft.mvvm_rx.models.User;
import com.vividmindsoft.mvvm_rx.results.LoginOutResult;

//Implementation 1
//Implementation 2 -> without LoginResult , just use LoginResultCodes ... Messages handle in viewModel !?
public class FakeService {

    public static LoginOutResult login(User user) {
//        Thread.sleep(2000);

        if (user.getLoggedIn()){
            return new LoginOutResult(LoginOutResultCodes.FAILED_BY_YOU_LOGGED_IN,R.string.login_failed_by_you_logged_in);
        } else if(!checkUserDataValidity(user)){

            return new LoginOutResult(LoginOutResultCodes.FAILED_BY_INVALID_PW_OR_MAIL,R.string.login_failed_by_invalid_pw_or_mail);
        }
        user.setLoggedIn(true);
        return new LoginOutResult(LoginOutResultCodes.SUCCESS,R.string.login_success);
    }
    public static LoginOutResult logout(User user) {

        if (!user.getLoggedIn()){
            return new LoginOutResult(LoginOutResultCodes.FAILED_BY_YOU_LOGGED_OUT,R.string.logout_failed_by_you_logged_out);
        }
        user.setLoggedIn(false);
        return new LoginOutResult(LoginOutResultCodes.SUCCESS,R.string.logout_success);
    }

    private static boolean checkUserDataValidity(User user){
        return !TextUtils.isEmpty(user.getEmail()) && Patterns.EMAIL_ADDRESS.matcher(user.getEmail()).matches() && user.getPassword().length() > 5;
    }

}
