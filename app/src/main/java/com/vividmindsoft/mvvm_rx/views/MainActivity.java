package com.vividmindsoft.mvvm_rx.views;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.vividmindsoft.mvvm_rx.R;
import com.vividmindsoft.mvvm_rx.databinding.ActivityMainBinding;
import com.vividmindsoft.mvvm_rx.enums.resultCodes.LoginOutResultCodes;
import com.vividmindsoft.mvvm_rx.results.LoginOutResult;
import com.vividmindsoft.mvvm_rx.viewModels.MainActivityViewModel;
import com.vividmindsoft.mvvm_rx.viewModels.factories.MainActivityViewModelFactory;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity{

    private ActivityMainBinding binding;
    private MainActivityViewModel viewModel;

    private Disposable loginDisposable;
    private Disposable logoutDisposable;

    private static final String ACTIVITY_LOG_TAG = "MainActivityLog";

    private void initDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this,new MainActivityViewModelFactory()).get(MainActivityViewModel.class);
        binding.setViewModel(viewModel);
    }

    private void initDisposables(){
        //subscribe on, kezdettől hat
        //observe on, arra ha ami mögötte van a láncba
        loginDisposable = viewModel.getLoginObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<LoginOutResult>() {

            //LoginResult
            @Override
            public void onNext(LoginOutResult loginResult) {
                Toast.makeText(MainActivity.this,  MainActivity.this.getString(loginResult.getMessageStrId()), Toast.LENGTH_SHORT).show();
                Log.d(ACTIVITY_LOG_TAG,"LoginDisposable_OnNext: "+loginResult.toString());
            }

            @Override
            public void onError(Throwable e) {
                //error has been ended the stream
                Toast.makeText(MainActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.d(ACTIVITY_LOG_TAG,"LoginDisposable_OnError: ");
            }

            @Override
            public void onComplete() {
                Log.d(ACTIVITY_LOG_TAG,"LoginDisposable_OnComplete: ");
            }
        });
        logoutDisposable = viewModel.getLogoutObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<LoginOutResult>() {
            @Override
            public void onNext(LoginOutResult loginResult) {
                Toast.makeText(MainActivity.this,  MainActivity.this.getString(loginResult.getMessageStrId()), Toast.LENGTH_SHORT).show();
                Log.d(ACTIVITY_LOG_TAG,"LoginDisposable_OnNext: "+loginResult.toString());
            }

            @Override
            public void onError(Throwable e) {
                //error has been ended the stream
                Log.d(ACTIVITY_LOG_TAG,"LogOutDisposable_OnError: ");
                Toast.makeText(MainActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onComplete() {
                Log.d(ACTIVITY_LOG_TAG,"LogOutDisposable_OnComplete: ");
            }
        });
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataBinding();
        initDisposables();
    }

    @Override
    protected void onDestroy() {
        if (!loginDisposable.isDisposed())
            loginDisposable.dispose();
        if (!logoutDisposable.isDisposed())
            logoutDisposable.dispose();

        Log.d(ACTIVITY_LOG_TAG,"OnDestroy");
        super.onDestroy();
    }

}
