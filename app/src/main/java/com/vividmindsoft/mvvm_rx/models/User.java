package com.vividmindsoft.mvvm_rx.models;

import android.databinding.BaseObservable;
import android.text.TextUtils;
import android.util.Patterns;

import java.util.Observable;

public class User {

    private String email = "";
    private String password = "";
    private Boolean isLoggedIn = false;

    public User(){

    }

    public Boolean getLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
