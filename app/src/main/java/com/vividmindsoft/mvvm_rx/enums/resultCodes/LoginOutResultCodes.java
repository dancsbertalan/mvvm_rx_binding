package com.vividmindsoft.mvvm_rx.enums.resultCodes;

public enum LoginOutResultCodes {
    SUCCESS("1001"),
    FAILED_BY_INVALID_PW_OR_MAIL("1002"),
    FAILED_BY_YOU_LOGGED_IN("1003"),
    FAILED_BY_YOU_LOGGED_OUT("1004");

    private String code;

    LoginOutResultCodes(String code){
        this.code = code;
    }

    public String code(){
        return code;
    }

    @Override
    public String toString() {
        return code;
    }
}
