package com.vividmindsoft.mvvm_rx.viewModels;

import android.arch.lifecycle.ViewModel;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.vividmindsoft.mvvm_rx.FakeService;
import com.vividmindsoft.mvvm_rx.enums.resultCodes.LoginOutResultCodes;
import com.vividmindsoft.mvvm_rx.models.User;
import com.vividmindsoft.mvvm_rx.results.LoginOutResult;

import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class MainActivityViewModel extends ViewModel {

    private Random rnd = new Random();

    private Subject<LoginOutResult> loginSubject = PublishSubject.create();
    private Subject<LoginOutResult> logoutSubject = PublishSubject.create();

    private TextWatcher emailTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            userModel.setEmail(s.toString());
        }
    };
    private TextWatcher passwordTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            userModel.setPassword(s.toString());
        }
    };

    private User userModel;
    //használjuk rx-es viewobservvable, flatmap -> eredmény amit kifele adunk


    public MainActivityViewModel() {
        userModel = new User();
    }

    public Observable<LoginOutResult> getLogoutObservable() {
        return logoutSubject;
    }
    public Observable<LoginOutResult> getLoginObservable() {
        return loginSubject; //asobservable
    }

    public TextWatcher getEmailTextWatcher() {
        return emailTextWatcher;
    }

    public TextWatcher getPasswordTextWatcher() {
        return passwordTextWatcher;
    }

    //replace rxandroid or other friend :D
    public void loginButtonClick(View view) {
        //Ha itt írsz "sleepet" akkor az a ui threaden megy, mert hozzá van rendelve
        loginSubject.onNext(FakeService.login(userModel));
    }

    public void logoutButtonClick(View view) {
        loginSubject.onNext(FakeService.logout(userModel));
    }
}
