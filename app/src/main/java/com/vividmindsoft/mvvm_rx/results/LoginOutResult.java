package com.vividmindsoft.mvvm_rx.results;

import com.vividmindsoft.mvvm_rx.enums.resultCodes.LoginOutResultCodes;

public class LoginOutResult {

    public LoginOutResult(LoginOutResultCodes resultCode, int messageStrId) {
        this.resultCode = resultCode;
        this.messageStrId = messageStrId;
    }

    private LoginOutResultCodes resultCode;
    private int messageStrId;

    public LoginOutResultCodes getResultCode() {
        return resultCode;
    }
    public int getMessageStrId() {
        return messageStrId;
    }

    @Override
    public String toString() {
        return "ResultCode: " + resultCode.toString() + " || MessageStrId: " + Integer.toString(messageStrId);
    }
}
